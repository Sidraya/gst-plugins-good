/*
 * Copyright (C) 2014 SUMOMO Computer Association
 *     Author: ayaka <ayaka@soulik.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#include "gstv4l2object.h"
#include "gstv4l2h264enc.h"
#include "gstv4l2h264codec.h"

#include <string.h>
#include <gst/gst-i18n-plugin.h>

GST_DEBUG_CATEGORY_STATIC (gst_v4l2_h264_enc_debug);
#define GST_CAT_DEFAULT gst_v4l2_h264_enc_debug


static GstStaticCaps src_template_caps =
GST_STATIC_CAPS ("video/x-h264, stream-format=(string) byte-stream, "
    "alignment=(string) au");

enum
{
  PROP_0,
  V4L2_STD_OBJECT_PROPS,
  PROP_BITRATE,
  PROP_GOPSIZE,
  PROP_I_PERIOD,
/* TODO add H264 controls
 * PROP_I_FRAME_QP,
 * PROP_P_FRAME_QP,
 * PROP_B_FRAME_QP,
 * PROP_MIN_QP,
 * PROP_MAX_QP,
 * PROP_8x8_TRANSFORM,
 * PROP_CPB_SIZE,
 * PROP_ENTROPY_MODE,
 * PROP_I_PERIOD,
 * PROP_LOOP_FILTER_ALPHA,
 * PROP_LOOP_FILTER_BETA,
 * PROP_LOOP_FILTER_MODE,
 * PROP_VUI_EXT_SAR_HEIGHT,
 * PROP_VUI_EXT_SAR_WIDTH,
 * PROP_VUI_SAR_ENABLED,
 * PROP_VUI_SAR_IDC,
 * PROP_SEI_FRAME_PACKING,
 * PROP_SEI_FP_CURRENT_FRAME_0,
 * PROP_SEI_FP_ARRANGEMENT_TYP,
 * ...
 * */
};

#define gst_v4l2_h264_enc_parent_class parent_class
G_DEFINE_TYPE (GstV4l2H264Enc, gst_v4l2_h264_enc, GST_TYPE_V4L2_VIDEO_ENC);

#define DEFAULT_PROP_BITRATE 	(500 * 1000)
#define DEFAULT_PROP_GOPSIZE 	(1800)
#define DEFAULT_PROP_I_PERIOD 	(30)

static void
gst_v4l2_h264_enc_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  GstV4l2H264Enc *self = GST_V4L2_H264_ENC (object);

  switch (prop_id) {
    case PROP_BITRATE:
      self->bitrate = g_value_get_uint (value);
      break;
    case PROP_GOPSIZE:
      self->gopsize = g_value_get_uint (value);
      break;
    case PROP_I_PERIOD:
      self->iperiod = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_v4l2_h264_enc_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec)
{
  GstV4l2H264Enc *self = GST_V4L2_H264_ENC (object);

  switch (prop_id) {
    case PROP_BITRATE:
      g_value_set_uint (value, self->bitrate);
      break;
    case PROP_GOPSIZE:
      g_value_set_uint (value, self->gopsize);
      break;
    case PROP_I_PERIOD:
      g_value_set_uint (value, self->iperiod);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_v4l2_h264_enc_init (GstV4l2H264Enc * self)
{
  /* Initializing H264 encoder extended controls parameter
   * with default values */
  self->bitrate = DEFAULT_PROP_BITRATE;
  self->gopsize = DEFAULT_PROP_GOPSIZE;
  self->iperiod = DEFAULT_PROP_I_PERIOD;
}

static void
gst_v4l2_h264_enc_get_ctrls (GstV4l2Object * v4l2object)
{
  struct v4l2_ext_controls ctrls;
  struct v4l2_ext_control controls[3];
  guint i;

  memset (&ctrls, 0, sizeof (ctrls));
  memset (controls, 0, sizeof (controls));

  ctrls.which = V4L2_CTRL_WHICH_CUR_VAL;
  ctrls.count = 3;
  ctrls.controls = controls;

  controls[0].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
  controls[1].id = V4L2_CID_MPEG_VIDEO_BITRATE;
  controls[2].id = V4L2_CID_MPEG_VIDEO_H264_I_PERIOD;

  if (v4l2object->ioctl (v4l2object->video_fd, VIDIOC_G_EXT_CTRLS, &ctrls)) {
    GST_DEBUG_OBJECT (v4l2object, "Failed to get extended "
        "controls for H264 encoder");
    return;
  } else {
    GST_LOG_OBJECT (v4l2object, "Extended controls "
        "for H264 encoder bitrate=[%d] gopsize=[%d] iperiod=[%d]",
        controls[0].value, controls[1].value, controls[2].value);
  }
}

static void
gst_v4l2_h264_enc_set_ctrls (GstV4l2Object * v4l2object, guint bitrate,
    guint gop_size, guint i_period)
{
  struct v4l2_ext_controls ctrls;
  struct v4l2_ext_control controls[3];
  guint i;

  memset (&ctrls, 0, sizeof (ctrls));
  memset (controls, 0, sizeof (controls));

  ctrls.which = V4L2_CTRL_WHICH_CUR_VAL;
  ctrls.count = 3;
  ctrls.controls = controls;

  controls[0].id = V4L2_CID_MPEG_VIDEO_GOP_SIZE;
  controls[0].value = gop_size;
  controls[1].id = V4L2_CID_MPEG_VIDEO_BITRATE;
  controls[1].value = bitrate;
  controls[2].id = V4L2_CID_MPEG_VIDEO_H264_I_PERIOD;
  controls[2].value = i_period;

  if (v4l2object->ioctl (v4l2object->video_fd, VIDIOC_S_EXT_CTRLS, &ctrls)) {
    GST_DEBUG_OBJECT (v4l2object, "Failed to set extended "
        "controls for encoder");
    return;
  }
}

static gboolean
gst_v4l2_h264_enc_start (GstV4l2VideoEnc * v4l2enc)
{
  GstV4l2H264Enc *self = GST_V4L2_H264_ENC (v4l2enc);

  gst_v4l2_h264_enc_set_ctrls (v4l2enc->v4l2output, self->bitrate,
      self->gopsize, self->iperiod);
  gst_v4l2_h264_enc_get_ctrls (v4l2enc->v4l2output);

  return GST_VIDEO_ENCODER_CLASS (parent_class)->start (v4l2enc);
}

static void
gst_v4l2_h264_enc_class_init (GstV4l2H264EncClass * klass)
{
  GstElementClass *element_class;
  GObjectClass *gobject_class;
  GstV4l2VideoEncClass *baseclass;
  GstVideoEncoderClass *video_encoder_class;

  parent_class = g_type_class_peek_parent (klass);

  element_class = (GstElementClass *) klass;
  gobject_class = (GObjectClass *) klass;
  baseclass = (GstV4l2VideoEncClass *) (klass);
  video_encoder_class = (GstVideoEncoderClass *) klass;

  GST_DEBUG_CATEGORY_INIT (gst_v4l2_h264_enc_debug, "v4l2h264enc", 0,
      "V4L2 H.264 Encoder");

  gst_element_class_set_static_metadata (element_class,
      "V4L2 H.264 Encoder",
      "Codec/Encoder/Video/Hardware",
      "Encode H.264 video streams via V4L2 API", "ayaka <ayaka@soulik.info>");

  gobject_class->set_property =
      GST_DEBUG_FUNCPTR (gst_v4l2_h264_enc_set_property);
  gobject_class->get_property =
      GST_DEBUG_FUNCPTR (gst_v4l2_h264_enc_get_property);

  video_encoder_class->start = GST_DEBUG_FUNCPTR (gst_v4l2_h264_enc_start);

  baseclass->codec_name = "H264";

  g_object_class_install_property (gobject_class, PROP_BITRATE,
      g_param_spec_uint ("bitrate", "Bitrate", "Bitrate in bit/sec",
          50 * 1000, 100 * 1000 * 1000, DEFAULT_PROP_BITRATE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_GOPSIZE,
      g_param_spec_uint ("gop-size", "Gop size", "Size of a group of "
          "picture starting with an IDR frame",
          1, 7200, DEFAULT_PROP_GOPSIZE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_I_PERIOD,
      g_param_spec_uint ("i-period", "I period", "I Frame Period",
          1, 600, DEFAULT_PROP_I_PERIOD,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

/* Probing functions */
gboolean
gst_v4l2_is_h264_enc (GstCaps * sink_caps, GstCaps * src_caps)
{
  return gst_v4l2_is_video_enc (sink_caps, src_caps,
      gst_static_caps_get (&src_template_caps));
}

void
gst_v4l2_h264_enc_register (GstPlugin * plugin, const gchar * basename,
    const gchar * device_path, gint video_fd, GstCaps * sink_caps,
    GstCaps * src_caps)
{
  const GstV4l2Codec *codec = gst_v4l2_h264_get_codec ();
  gst_v4l2_video_enc_register (plugin, GST_TYPE_V4L2_H264_ENC,
      "h264", basename, device_path, codec, video_fd, sink_caps,
      gst_static_caps_get (&src_template_caps), src_caps);
}
